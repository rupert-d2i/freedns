#!/usr/bin/env python

import requests
from time import sleep
from requests.auth import HTTPBasicAuth
from lxml import html
import socket
import urllib2
from datetime import datetime
import logging
import getpass

'''
This is a ipaddress scraper to assist helping users of a NETGEAR DG834GV DG834Gv4 etc
It only has the ability to point to DynDNS natively no other providers are available
hence I created this script to scrape the Netgear UI page to get the public URI
This will then send (in my case) a message parcel to freedns (a free dynamic dns service)
and update a public IP address associated with the site *only* if it is different
this will ensure you dont send nusence packets trying to update if already correct.

This can be called from another file with the following contents you might get away with only
changing the password and freedns hash value...

#!/usr/bin/env python

from syncdns import URLSync

if __name__ == '__main__':

    DNSURL = 'rupert160.privatedns.org'
    DNSURLUpdate = 'https://freedns.afraid.org/dynamic/update.php?esome_large_hash_arranged_with_freedns=='
    routerAddress = 'http://192.168.0.1/setup.cgi?next_file=s_status.htm'
    logoutPage = 'http://192.168.0.1/setup.cgi?todo=logout'
    routerUser = 'admin'
    routerPassword = 'secret'
    logFile = '~/syncdns.log'
    URLSync(
        DNSURL, DNSURLUpdate, routerAddress, routerUser, routerPassword, logFile, logoutPage
    ).sync_ip()
'''

class URLSync(object):

    def __init__(self, DNSURL='', DNSURLUpdate='', routerAddress='', routerUser='', routerPassword='', logFile='', logoutPage=''
                 ):
        self.DNSURL = DNSURL
        self.DNSURLUpdate = DNSURLUpdate
        self.routerAddress = routerAddress
        self.routerUser = routerUser
        self.routerPassword = routerPassword
        self.logFile = logFile
        self.logoutPage = logoutPage

    def get_actual_ip(self):
        x = 3
        rows = None
        while x > 0:
            try:
                page = requests.get(self.routerAddress,
                                    auth=HTTPBasicAuth(self.routerUser, self.routerPassword))
                requests.get(self.logoutPage)
            except requests.exceptions.RequestException as e1:
                # print e1
                sys.exit(1)

            try:
                tree = html.fromstring(page.content)
            except Exception as e2:
                # print e2
                sys.exit(2)

            try:
                rows = tree.xpath('//body')[0].xpath('//table')[0].xpath('//tr')
            except IndexError as e3:
                if x > 0:
                    x -= 1
                    continue
                else:
                    raise ValueError('Content of tree is: %s' % page.content)
                    # print e3
                    sys.exit(3)
            else:
                break

        for row in rows:
            if "IP Address" in row.xpath('td[1]//text()')[0]:
                return row.xpath('td[2]//text()')[0]
                break

    def sync_ip(self):
        ip_published = socket.getaddrinfo(self.DNSURL, 22)[0][4][0]
        ip_actual = self.get_actual_ip()
        if ip_published != ip_actual:
            FORMAT = '%(asctime)-15s %(clientip)s %(user)-8s %(message)s'
            logging.basicConfig(
                format=FORMAT, datefmt='%F %T', filename=self.logFile)
            d = {'clientip': socket.gethostname(), 'user': getpass.getuser()}
            logger = logging.getLogger()
            logger.warning('Attempting to change published ip from %s to %s',
                           ip_published, ip_actual, extra=d)
            result = requests.get(self.DNSURLUpdate, verify=False)
        else:
            FORMAT = '%(asctime)-15s %(clientip)s %(user)-8s %(message)s'
            logging.basicConfig(
                format=FORMAT, datefmt='%F %T', filename=self.logFile)
            d = {'clientip': socket.gethostname(), 'user': getpass.getuser()}
            logger = logging.getLogger()
            logger.warning('No change in published ip of: %s as actual is: %s',
                           ip_published, ip_actual, extra=d)
